<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>消費税計算</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>

    <pre><?php
    var_dump($_GET);
    ?></pre>
<?php
 $te1 = $_GET['text1'];
 $te2 = $_GET['text2'];
 $te3 = $_GET['text3'];
 $te4 = $_GET['text4'];
 $te5 = $_GET['text5'];
 $ka1 = $_GET['yen1'];
 $ka2 = $_GET['yen2'];
 $ka3 = $_GET['yen3'];
 $ka4 = $_GET['yen4'];
 $ka5 = $_GET['yen5'];
 $ko1 = $_GET['kosuu1'];
 $ko2 = $_GET['kosuu2'];
 $ko3 = $_GET['kosuu3'];
 $ko4 = $_GET['kosuu4'];
 $ko5 = $_GET['kosuu5'];
 $zei1 = $_GET['tiekku1'];
 $zei2 = $_GET['tiekku2'];
 $zei3 = $_GET['tiekku3'];
 $zei4 = $_GET['tiekku4'];
 $zei5 = $_GET['tiekku5'];
 $gou1 = $ka1* $ko1* (($zei1*0.01)+1);
 $gou2 = $ka2* $ko2* (($zei2*0.01)+1);
 $gou3 = $ka3* $ko3* (($zei3*0.01)+1);
 $gou4 = $ka4* $ko4* (($zei4*0.01)+1);
 $gou5 = floor ($ka5* $ko5* (($zei5*0.01)+1));
 $gou6 = $gou1+$gou2+$gou3+$gou4+$gou5;
 ?>

    <table border="1" style="border-collapse:collapse;">
      <tr>
        <th>商品名</th>
        <th>価格（単位：円、税抜き）</th>
        <th>個数</th>
        <th>税率</th>
        <th>小計（単位：円）</th>
      </tr>
      <tr>
        <td><?=$te1?></td>
        <td><?=$ka1?></td>
        <td><?=$ko1?></td>
        <td><?=$zei1?></td>
        <td><?=$gou1?></td>
      </tr>
      <tr>
        <td><?=$te2?></td>
        <td><?=$ka2?></td>
        <td><?=$ko2?></td>
        <td><?=$zei2?></td>
        <td><?=$gou2?></td>
      </tr>
      <tr>
        <td><?=$te3?></td>
        <td><?=$ka3?></td>
        <td><?=$ko3?></td>
        <td><?=$zei3?></td>
        <td><?=$gou3?></td>
      </tr>
      <tr>
        <td><?=$te4?></td>
        <td><?=$ka4?></td>
        <td><?=$ko4?></td>
        <td><?=$zei4?></td>
        <td><?=$gou4?></td>
      </tr>
      <tr>
        <td><?=$te5?></td>
        <td><?=$ka5?></td>
        <td><?=$ko5?></td>
        <td><?=$zei5?></td>
        <td><?=$gou5?></td>
      </tr>
      <tr>
        <td colspan=4>合計</td>
        <td style="text-align: right;"><?=$gou6?></td>
      </tr>

    </table>
  </body>
</html>
