<!DOCTYPE html>
<html>
<?php include('./include/head_title.php');?>
      <title>社員詳細編集</title>
</head>
<script type="text/javascript">
<!--
function conf(){
  //名前からスペースの除去
  var xyz ;
  xyz= document.touroku.onamae.value;
  document.touroku.onamae.value =  xyz.replace(/\s+/g,"");

  //年齢上限下限チェック
  if(document.touroku.nenrei.value == ""){
    alert("年齢は必須です");
    return false;
  }else if(document.touroku.nenrei.value <1 ){
    alert("1歳以上を入力してください");
    return false;
  }else if(document.touroku.nenrei.value >130 ){
    alert("130歳以下を入力してください");
    return false;
  }
  //必須チェック
  if(document.touroku.onamae.value == ""){
    alert("名前を入力してください");
    return false;
  }
  if(document.touroku.syussinti.value == ""){
    alert("出身地を入力してください");
    return false;
  }
  //確認ダイアログ
  if(window.confirm('追加を行います。よろしいでしょうか？')){
    document.touroku.submit();
  }else{
  }
}
-->
</script>
<link rel="stylesheet" href="./include/style.css">

<?php
  include('./include/include.php');
  $temp_id = "";
  if(isset($_POST['id']) AND $_POST['id'] !=""){
    $temp_id = $_POST['id'];
  }


  $pdo =  initDB();
  $query_str = "SELECT meibo.id,meibo.namae,meibo.syussin,meibo.seibetu,meibo.nennrei,meibo.syozoku,
                       meibo.yakusyoku
                FROM `meibo`

                WHERE meibo.id = ". $temp_id;


  // echo $query_str;
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();

  $pdo =  initDB();
  $sec_query_str = "SELECT section_master.bangou,section_master.section
                    FROM `section_master`
                    WHERE 1";
  $sql = $pdo->prepare($sec_query_str);
  $sql->execute();
  $sec_result = $sql->fetchAll();

  $yaku_query_str = "SELECT yakusyoku_master.number,yakusyoku_master.yakusyokumei
                    FROM `yakusyoku_master`
                    WHERE 1";
  $sql = $pdo->prepare($yaku_query_str);
  $sql->execute();
  $yaku_result = $sql->fetchAll();

  ?>



  <html>
      <?php include('./include/header.php') ?>
      <form method="POST" action="update02.php" name="touroku">



      <table id="table_bo" class="main_tbl">
        <tr>
          <th>名前</th>
          <td> <input type ="text"  required name="onamae" maxlength="30" value=
             "<?php
             echo $result[0]['namae'];
             ?>">
           </td>
        </tr>
        <tr>
          <th>出身地</th>
          <td>
            <select name="syussinti" required >
            <option value="">すべて</option>

              <?php
              foreach($prefecture_array AS $key=>$value){
                if($key == $result[0]['syussin']){
                  echo "<option value='" . $key . "' selected>" . $value . "</option>";
                }else{
                  echo "<option value='" . $key . "'>" . $value . "</option>";
                }
                // echo "<option value='" . $key . "'>" . $value . "</option><br>";
              }
              ?>
            </td>
            </select>
          </td>
        </tr>
        <tr>
          <th>性別</th>
          <td>
            <input type="radio" name="sex" value="1"<?php if($result[0]['seibetu'] == "1"){echo " checked";}?>>男
            <input type="radio" name="sex" value="2"<?php if($result[0]['seibetu'] == "2"){echo " checked";}?>>女
          </td>
        </tr>
        <tr>
          <th>年齢</th>
          <td> <input type ="number" required name="nenrei" max="99" min="1"
                value="<?php
                echo $result[0]['nennrei'];
                ?>"></td>
        </tr>
        <tr>
          <th>所属部署</th>
          <td>
            <?php
            foreach($sec_result as $each){
              echo "<input type='radio' name='busyo' value='" . $each['bangou'] . "' ";
              if($result[0]['syozoku'] == $each['bangou']){
                echo "checked";
              }
              echo ">" . $each['section'];
            }
            ?>
          </td>
        </tr>
        <tr>
          <th>役職</th>
          <td>
            <?php
            foreach($yaku_result as $each){
              echo "<input type='radio' name='yakusyoku' value='" . $each['number'] . "' ";
              if($result[0]['yakusyoku'] == $each['number']){
                echo "checked";
              }
              echo ">" . $each['yakusyokumei'];
            }
            ?>
              </td>
        </tr>
      </table>


      <input type="hidden" value="<?=$temp_id;?>" name="id">
      <div class="submit2_form">
      <input type="button" value="登録" onclick="conf(); "class="button_form"><input type="reset" value="リセット">
      </div>
    </form>
