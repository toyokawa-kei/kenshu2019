<!DOCTYPE html>

<html>
<?php include('./include/head_title.php');?>
      <title>社員一覧画面</title>
<script type="text/javascript">
<!--
function space_del(){
  //スペースの除去
  var xyz ;
  xyz= document.nyuuryoku.onamae.value;
  document.nyuuryoku.onamae.value =  xyz.replace(/\s+/g,"");
}
function naiyou_sakujyo(){
  document.nyuuryoku.onamae.value = "";
  document.nyuuryoku.seibetu.value = "0";
  document.nyuuryoku.busyo.value = "0";
  document.nyuuryoku.yakusyoku.value = "0";
}
    -->
</script>
</head>
<?php

  include('./include/include.php');

  $pdo =  initDB();
  $query_str = "SELECT m.id,m.namae,se.section,ya.yakusyokumei
                FROM `meibo` AS m
                LEFT JOIN yakusyoku_master AS ya ON m.yakusyoku = ya.number
                LEFT JOIN section_master AS se ON m.syozoku = se.bangou
                WHERE 1";
  if(isset($_GET['onamae']) AND $_GET['onamae'] != ""){
    $query_str .= " AND m.namae LIKE '%" . $_GET['onamae'] . "%' " ;
  }
  if(isset($_GET['seibetu']) AND $_GET['seibetu'] != "0"){
    $query_str .= " AND m.seibetu = " . $_GET['seibetu'];
  }
  if(isset($_GET['busyo']) AND $_GET['busyo'] != "0"){
    $query_str .= " AND m.syozoku = " . $_GET['busyo'];
  }
  if(isset($_GET['yakusyoku']) AND $_GET['yakusyoku'] != "0"){
    $query_str .= " AND m.yakusyoku = " . $_GET['yakusyoku'];
  }



  // echo $query_str;
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();


  $sec_query_str = "SELECT section_master.bangou,section_master.section
                    FROM `section_master`
                    WHERE 1";
  $sql = $pdo->prepare($sec_query_str);
  $sql->execute();
  $sec_result = $sql->fetchAll();

  $yaku_query_str = "SELECT yakusyoku_master.number,yakusyoku_master.yakusyokumei
                    FROM `yakusyoku_master`
                    WHERE 1";
  $sql = $pdo->prepare($yaku_query_str);
  $sql->execute();
  $yaku_result = $sql->fetchAll();
  ?>

  <?php include('./include/header.php') ?>
      <body>

      <form method="GET" action="index.php" name="nyuuryoku">
      <div class="serch_form">
        <div class="namae_input">
          <b>名前： </b><input type ="text"  name="onamae" maxlength="30" value ="<?php if(isset($_GET['onamae'])){ echo $_GET['onamae'];}?>"><br>
        </div>

      <?php
        $temp_seibetu = "";
        if(isset($_GET['seibetu']) AND $_GET['seibetu'] != ""){
          $temp_seibetu = $_GET['seibetu'];
        }
      ?>
          <div class="sonota input">
      <b>性別： </b><select name="seibetu">
            <option value="0">すべて</option>
            <option value="1" <?php if($temp_seibetu == "1"){echo "selected";} ?>>男</option>
            <option value="2" <?php if($temp_seibetu == "2"){echo "selected";} ?>>女</option>
            </select>

      <?php
        $temp_busyo = "";
        if(isset($_GET['busyo']) AND $_GET['busyo'] != ""){
           $temp_busyo = $_GET['busyo'];
        }
      ?>


      <b>部署： </b><select name="busyo">
        <option value="0">すべて</option>
        <?php
        foreach($sec_result as $each){
          echo "<option value='" . $each['bangou'] . "' ";
          if($temp_busyo == $each['bangou']){
            echo "selected";
          }
          echo ">" . $each['section'] . "</option>";
        }
          ?>

            </select>
        <?php
          $temp_yakusyoku = "";
          if(isset($_GET['yakusyoku']) AND $_GET['yakusyoku'] != ""){
            $temp_yakusyoku = $_GET['yakusyoku'];
          }
        ?>


      <b>役職： </b><select name="yakusyoku">
            <option value="0">すべて</option>
            <?php
            foreach($yaku_result as $each){
              echo "<option value='" . $each['number'] . "' ";
              if($temp_yakusyoku == $each['number']){
                echo "selected";
              }
              echo ">" . $each['yakusyokumei'] . "</option>";
            }
              ?>

                </select>
            

            <p></p>
          </div><!--sonotaの終わり-->
      </div><!--serchform終わり-->
          <div class="ta_c">
            <input type="submit" value="検索" onclick="space_del();">
            <input type="button" value="リセットする" onclick="naiyou_sakujyo();">
          </div>
          </form>
          <hr>






<div class="result_span">
      検索結果：
 <?php
  echo COUNT($result)
  ?>
</div>

  <table id="table_bo" class="main_tbl">
  <tr>
<?php
if(COUNT($result) == "0"){
  echo "<th>該当する社員はありません</th>";
}else{
      echo "<th>社員ID</th>
            <th>名前</th>
            <th>部署</th>
            <th>役職</th>";
}
?>
  </tr>
      <?php

      foreach($result as $each){
        echo "<tr><td class='id_row'>" . $each['id'] . "</td>" .
             "<td><a href='./detail01.php?id=" . $each['id'] . "'>" . $each['namae'] . "</a></td>" .
             "<td>" . $each['section'] . "</td>" .
             "<td>" . $each['yakusyokumei'] . "</td></tr>" ;
      }


      ?>
    </table>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    </body>
  </html>
